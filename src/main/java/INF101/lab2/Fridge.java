package INF101.lab2;

import java.util.NoSuchElementException;
import java.util.List;
import java.util.ArrayList;

public class Fridge implements IFridge {
	
	int count = 0;
	//Array is optimal data structure as we don't exceed 20 items.
	FridgeItem[] items = new FridgeItem[20];

	//Returns number of items in fridge.
	@Override
	public int nItemsInFridge() {
		
		return count;
	}

	/* Returns length of fixed array (20). Fridge size can be altered.
	 */
	@Override
	public int totalSize() {
		return items.length;
	}
	
	
	/* Decides whether there is space in the fridge, and updates variables
	 * and array accordingly if there is.
	 */
	@Override
	public boolean placeIn(FridgeItem item) {
		
		if(count < totalSize()) {
			items[count] = item;
			count++;
			return true;
		} else {
			return false;
		}
	}
	
	/* Iterates through FridgeItem array and checks for the argument. If found,
	 * replaces value at current index with value of last index, and changes
	 * last valid value to null.
	 */
	@Override
	public void takeOut(FridgeItem item) {
		
		for(int i = 0; i < count; i++) {
		 	if(items[i].equals(item)) {
		 		count--;
		 		items[i] = items[count];
		 		items[count] = null;
		 		return;
		 	}
		}
		throw new NoSuchElementException();
	}
	
	//Removes all items from FridgeItem array.
	@Override
	public void emptyFridge() {
		
		for(int i = 0; i < count; i++) {
			items[i] = null;
		}
		count = 0;
	}
	
	@Override
	/* Removes all items from FridgeItem array whose expiration date is
	 * earlier than current date.
	 */
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
		
		for(int i = 0; i < count; i++) {
			if(items[i].hasExpired()) {
				expiredItems.add(items[i]);
				count--;
				items[i] = items[count];
				items[count] = null;
			}
		}
		return expiredItems;
	}
}
 